#!/bin/sh
# Copyright 2022 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT
#
# This is a mmdebstrap customize hook that configures automatic root login on a
# serial console. It also parses the TERM kernel cmdline and passes it as
# TERM to agetty.

set -eu

TARGET=$1

UNIT=serial-getty@.service

mkdir "$TARGET/etc/systemd/system/$UNIT.d"

(
	echo '[Service]'
	printf '%s\n' 'ExecStartPre=/bin/sed -n -e "s/^\\(.* \\)\\?\\(TERM=[^ ]*\\).*/\\2/w/run/debvmterm" /proc/cmdline'
	echo 'EnvironmentFile=-/run/debvmterm'
	echo 'ExecStart='
	sed -n 's,^ExecStart=-/sbin/agetty ,&-a root ,p' "$TARGET/lib/systemd/system/$UNIT"
) > "$TARGET/etc/systemd/system/$UNIT.d/autologin.conf"
